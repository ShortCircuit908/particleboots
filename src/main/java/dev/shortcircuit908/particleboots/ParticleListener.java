package dev.shortcircuit908.particleboots;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;

public class ParticleListener implements Listener {
	private final ParticleBootsPlugin plugin;
	private final double min_delta = Math.pow(0.2, 2);
	
	public ParticleListener(ParticleBootsPlugin plugin) {
		this.plugin = plugin;
	}
	
	@SuppressWarnings("ConstantConditions")
	@EventHandler
	public void spawnParticles(final PlayerMoveEvent event) {
		Location from = event.getFrom();
		Location to = event.getTo();
		if (to == null || from.getWorld() == null || !from.getWorld().equals(to.getWorld()) ||
				from.distanceSquared(to) < min_delta) {
			return;
		}
		Player player = event.getPlayer();
		if (player.isSneaking() && !player.isFlying()) {
			return;
		}
		ItemStack boots = player.getInventory().getBoots();
		if (boots == null || !boots.hasItemMeta()) {
			return;
		}
		int level = boots.getItemMeta().getEnchantLevel(plugin.getParticleEnchantment());
		if (level <= 0 || level >= BootParticle.values().length) {
			return;
		}
		BootParticle particle = BootParticle.values()[level - 1];
		if (!player.hasPermission(BootParticle.WILDCARD_PERMISSION) &&
				!player.hasPermission(particle.getPermission())) {
			return;
		}
		particle.getSpawner().spawnParticles(particle.getParticle(), from);
	}
}
