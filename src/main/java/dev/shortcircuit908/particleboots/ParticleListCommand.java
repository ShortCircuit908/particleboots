package dev.shortcircuit908.particleboots;

import com.google.common.base.Joiner;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ParticleListCommand implements CommandExecutor {
	private static final Pattern page_pattern = Pattern.compile("^p:(\\d+)$", Pattern.CASE_INSENSITIVE);
	private static final int results_per_page = 9;
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		List<String> args_list = new LinkedList<>(Arrays.asList(args));
		
		int page = 1;
		Iterator<String> arg_iterator = args_list.listIterator();
		while (arg_iterator.hasNext()) {
			Matcher matcher = page_pattern.matcher(arg_iterator.next());
			if (matcher.find()) {
				page = Integer.parseInt(matcher.group(1));
				arg_iterator.remove();
				break;
			}
		}
		page = Math.max(1, page);
		
		final String search = args_list.isEmpty() ? null : args_list.get(0).toLowerCase();
		
		List<BootParticle> particles = new ArrayList<>(BootParticle.values().length);
		for (BootParticle particle : BootParticle.values()) {
			if ((sender.hasPermission(BootParticle.WILDCARD_PERMISSION) ||
					sender.hasPermission(particle.getPermission())) &&
					(search == null || search.isEmpty() || particle.name().toLowerCase().contains(search) ||
							particle.getAliases()
									.stream()
									.reduce(false, (b, s) -> s.toLowerCase().contains(search), Boolean::logicalOr))) {
				particles.add(particle);
			}
		}
		
		particles.sort(Comparator.naturalOrder());
		
		if (sender instanceof Player) {
			int total_pages = (int) Math.ceil(particles.size() / (double) results_per_page);
			
			if (particles.size() > results_per_page) {
				particles = particles.subList(
						(page - 1) * results_per_page,
						Math.min((page - 1) * results_per_page + results_per_page, particles.size())
				);
			}
			
			TextComponent header = new TextComponent("Available particles (pg ");
			header.setColor(net.md_5.bungee.api.ChatColor.GOLD);
			
			TextComponent cur_page = new TextComponent(Integer.toString(page));
			cur_page.setColor(net.md_5.bungee.api.ChatColor.RED);
			
			TextComponent total_page = new TextComponent(Integer.toString(total_pages));
			total_page.setColor(net.md_5.bungee.api.ChatColor.RED);
			
			header.addExtra(cur_page);
			header.addExtra("/");
			header.addExtra(total_page);
			header.addExtra(") ");
			
			TextComponent back_button = new TextComponent("«");
			if (page > 1) {
				String to_run = "/" + label + " p:" + (page - 1) + " " + Joiner.on(" ").join(args_list);
				back_button.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, to_run.trim()));
				back_button.setColor(net.md_5.bungee.api.ChatColor.RED);
				back_button.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text("Previous page")));
			}
			
			TextComponent forward_button = new TextComponent("»");
			if (page < total_pages) {
				String to_run = "/" + label + " p:" + (page + 1) + " " + Joiner.on(" ").join(args_list);
				forward_button.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, to_run.trim()));
				forward_button.setColor(net.md_5.bungee.api.ChatColor.RED);
				forward_button.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text("Next page")));
			}
			
			header.addExtra(back_button);
			header.addExtra(new TextComponent("|"));
			header.addExtra(forward_button);
			
			
			BaseComponent[] components = new BaseComponent[particles.size()];
			for(int i = 0; i < particles.size(); i++){
				BootParticle particle = particles.get(i);
				components[i] = new TextComponent(particle.name().toLowerCase());
				components[i].setColor(net.md_5.bungee.api.ChatColor.AQUA);
				
				if(!particle.getAliases().isEmpty()){
					Set<String> aliases =
							particle.getAliases().stream().map(String::toLowerCase).collect(Collectors.toSet());
					BaseComponent extra = new TextComponent(" (" + Joiner.on(", ").join(aliases) + ")");
					extra.setColor(net.md_5.bungee.api.ChatColor.BLUE);
					components[i].addExtra(extra);
				}
				
				components[i].setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND,
						"/particleboots " + particle.name()));
				components[i].setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text("Click to enchant")));
			}
			
			sender.spigot().sendMessage(header);
			for(BaseComponent component : components){
				sender.spigot().sendMessage(component);
			}
		}
		else {
			List<String> messages = new ArrayList<>(particles.size());
			for(BootParticle particle : particles){
				String line = ChatColor.AQUA + particle.name().toLowerCase();
				Set<String> aliases =
						particle.getAliases().stream().map(String::toLowerCase).collect(Collectors.toSet());
				if (!aliases.isEmpty()) {
					line += ChatColor.BLUE + " (" + Joiner.on(", ").join(aliases) + ")";
				}
				messages.add(line);
			}
			sender.sendMessage(
					ChatColor.GOLD + "Available particles (" + ChatColor.RED + particles.size() + ChatColor.GOLD +
							" total)");
			sender.sendMessage(messages.toArray(new String[0]));
		}
		return true;
	}
}
