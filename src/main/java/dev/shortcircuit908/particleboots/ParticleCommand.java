package dev.shortcircuit908.particleboots;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ParticleCommand implements CommandExecutor, TabCompleter {
	private final ParticleBootsPlugin plugin;
	
	public ParticleCommand(ParticleBootsPlugin plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "This command must be executed by a player");
			return true;
		}
		if (args.length < 1) {
			sender.sendMessage(ChatColor.RED + "Too few arguments");
			return false;
		}
		Player player = (Player) sender;
		ItemStack held_item = player.getInventory().getItemInMainHand();
		if (held_item.getType().isAir()) {
			sender.sendMessage(ChatColor.RED + "You are not holding an item");
			return true;
		}
		if (!plugin.getParticleEnchantment().canEnchantItem(held_item)) {
			sender.sendMessage(ChatColor.RED + "This item cannot be enchanted with particles");
			return true;
		}
		try {
			BootParticle particle = BootParticle.get(args[0]);
			if (!sender.hasPermission(BootParticle.WILDCARD_PERMISSION) &&
					!sender.hasPermission(particle.getPermission())) {
				sender.sendMessage(ChatColor.RED + "You do not have permission to use that particle");
				return true;
			}
			held_item.addUnsafeEnchantment(plugin.getParticleEnchantment(), particle.ordinal() + 1);
			sender.sendMessage(
					ChatColor.GOLD + "Your item has been enchanted with " + ChatColor.AQUA + particle.name() +
							ChatColor.GOLD + " particles!");
		}
		catch (IllegalArgumentException e) {
			sender.sendMessage(ChatColor.RED + "No particle named " + ChatColor.DARK_RED + args[0]);
			return true;
		}
		return true;
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		List<String> suggestions = new ArrayList<>(BootParticle.values().length);
		for (BootParticle particle : BootParticle.values()) {
			if ((sender.hasPermission(BootParticle.WILDCARD_PERMISSION) ||
					sender.hasPermission(particle.getPermission())) && (args.length == 0 || args[0].isEmpty() ||
					particle.name().toLowerCase().startsWith(args[0].toLowerCase()))) {
				suggestions.add(particle.name().toLowerCase());
				suggestions.addAll(particle.getAliases().stream().map(String::toLowerCase).collect(Collectors.toSet()));
			}
		}
		suggestions.sort(Comparator.naturalOrder());
		return suggestions;
	}
}
