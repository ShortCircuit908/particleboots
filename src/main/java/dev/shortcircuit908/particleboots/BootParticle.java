package dev.shortcircuit908.particleboots;

import dev.shortcircuit908.particleboots.particles.ParticleSpawnerRainbowDust;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public enum BootParticle {
	EXPLOSION_NORMAL(Particle.EXPLOSION_NORMAL, new DefaultOffsetParticleSpawner(1)),
	EXPLOSION_LARGE(Particle.EXPLOSION_LARGE, new DefaultParticleSpawner(1)),
	EXPLOSION_HUGE(Particle.EXPLOSION_HUGE, new DefaultParticleSpawner(1)),
	FIREWORKS_SPARK(Particle.FIREWORKS_SPARK, new DefaultParticleSpawner(1), "fireworks", "firework", "spark"),
	FIREWORKS_SPARK_SPRAY(Particle.FIREWORKS_SPARK,
			new DefaultExtraParticleSpawner(1, 0.1),
			"fireworks_spray",
			"firework_spray",
			"spark_spray"
	),
	FIREWORKS_SPARK_CLUSTER(Particle.FIREWORKS_SPARK,
			new DefaultOffsetParticleSpawner(8),
			"fireworks_cluster",
			"firework_cluster",
			"spark_cluster"
	),
	WATER_BUBBLE(Particle.WATER_BUBBLE, new DefaultParticleSpawner(1), "air_bubble"),
	WATER_SPLASH(Particle.WATER_SPLASH, new DefaultParticleSpawner(1), "splash"),
	WATER_WAKE(Particle.WATER_WAKE, new DefaultParticleSpawner(1), "wake"),
	SUSPENDED(Particle.SUSPENDED, new DefaultOffsetParticleSpawner(16), "void"),
	SUSPENDED_DEPTH(Particle.SUSPENDED_DEPTH, new DefaultOffsetParticleSpawner(16), "void_alt"),
	CRIT(Particle.CRIT, new DefaultParticleSpawner(1)),
	CRIT_MAGIC(Particle.CRIT_MAGIC, new DefaultParticleSpawner(1), "magic_crit"),
	SMOKE_NORMAL(Particle.SMOKE_NORMAL, new DefaultOffsetParticleSpawner(8), "smoke", "small_smoke", "smoke_small"),
	SMOKE_LARGE(Particle.SMOKE_LARGE, new DefaultParticleSpawner(1), "big_smoke", "smoke_big"),
	SPELL(Particle.SPELL, new DefaultParticleSpawner(4)),
	SPELL_INSTANT(Particle.SPELL_INSTANT, new DefaultParticleSpawner(4), "instant_spell"),
	SPELL_MOB(Particle.SPELL_MOB, new DefaultParticleSpawner(4), "bubbles", "rainbow_bubbles"),
	SPELL_MOB_AMBIENT(Particle.SPELL_MOB_AMBIENT,
			new DefaultParticleSpawner(4),
			"clearbubbles",
			"clear_rainbow_bubbles"
	),
	SPELL_WITCH(Particle.SPELL_WITCH, new DefaultParticleSpawner(4)),
	DRIP_WATER(Particle.DRIP_WATER, new DefaultParticleSpawner(1)),
	DRIP_WATER_CLUSTER(Particle.DRIP_WATER, new DefaultOffsetParticleSpawner(8)),
	DRIP_LAVA(Particle.DRIP_LAVA, new DefaultParticleSpawner(1)),
	DRIP_LAVA_CLUSTER(Particle.DRIP_LAVA, new DefaultOffsetParticleSpawner(8)),
	VILLAGER_ANGRY(Particle.VILLAGER_ANGRY, new DefaultOffsetParticleSpawner(4)),
	VILLAGER_HAPPY(Particle.VILLAGER_HAPPY, new DefaultParticleSpawner(1)),
	TOWN_AURA(Particle.TOWN_AURA, new DefaultOffsetParticleSpawner(16)),
	NOTE(Particle.NOTE, new DefaultExtraParticleSpawner(1, 4), "notes", "music"),
	PORTAL(Particle.PORTAL, new DefaultOffsetParticleSpawner(16)),
	ENCHANTMENT_TABLE(Particle.ENCHANTMENT_TABLE, new DefaultOffsetParticleSpawner(8), "magic", "runes"),
	FLAME(Particle.FLAME, new DefaultOffsetParticleSpawner(8), "fire"),
	LAVA(Particle.LAVA, new DefaultParticleSpawner(1)),
	CLOUD(Particle.CLOUD, new DefaultParticleSpawner(1)),
	DUST_WHITE(Particle.REDSTONE, new DefaultOffsetParticleSpawner(16, new Particle.DustOptions(Color.WHITE, 1.0f))),
	DUST_SILVER(Particle.REDSTONE, new DefaultOffsetParticleSpawner(16, new Particle.DustOptions(Color.SILVER, 1.0f))),
	DUST_GRAY(Particle.REDSTONE, new DefaultOffsetParticleSpawner(16, new Particle.DustOptions(Color.GRAY, 1.0f))),
	DUST_BLACK(Particle.REDSTONE, new DefaultOffsetParticleSpawner(16, new Particle.DustOptions(Color.BLACK, 1.0f))),
	DUST_RED(Particle.REDSTONE, new DefaultOffsetParticleSpawner(16, new Particle.DustOptions(Color.RED, 1.0f))),
	DUST_MAROON(Particle.REDSTONE, new DefaultOffsetParticleSpawner(16, new Particle.DustOptions(Color.MAROON, 1.0f))),
	DUST_YELLOW(Particle.REDSTONE, new DefaultOffsetParticleSpawner(16, new Particle.DustOptions(Color.YELLOW, 1.0f))),
	DUST_OLIVE(Particle.REDSTONE, new DefaultOffsetParticleSpawner(16, new Particle.DustOptions(Color.OLIVE, 1.0f))),
	DUST_LIME(Particle.REDSTONE, new DefaultOffsetParticleSpawner(16, new Particle.DustOptions(Color.LIME, 1.0f))),
	DUST_GREEN(Particle.REDSTONE, new DefaultOffsetParticleSpawner(16, new Particle.DustOptions(Color.GREEN, 1.0f))),
	DUST_AQUA(Particle.REDSTONE, new DefaultOffsetParticleSpawner(16, new Particle.DustOptions(Color.AQUA, 1.0f))),
	DUST_TEAL(Particle.REDSTONE, new DefaultOffsetParticleSpawner(16, new Particle.DustOptions(Color.TEAL, 1.0f))),
	DUST_BLUE(Particle.REDSTONE, new DefaultOffsetParticleSpawner(16, new Particle.DustOptions(Color.BLUE, 1.0f))),
	DUST_NAVY(Particle.REDSTONE, new DefaultOffsetParticleSpawner(16, new Particle.DustOptions(Color.NAVY, 1.0f))),
	DUST_FUCHSIA(Particle.REDSTONE,
			new DefaultOffsetParticleSpawner(16, new Particle.DustOptions(Color.FUCHSIA, 1.0f))
	),
	DUST_PURPLE(Particle.REDSTONE, new DefaultOffsetParticleSpawner(16, new Particle.DustOptions(Color.PURPLE, 1.0f))),
	DUST_ORANGE(Particle.REDSTONE, new DefaultOffsetParticleSpawner(16, new Particle.DustOptions(Color.ORANGE, 1.0f))),
	DUST_RAINBOW(Particle.REDSTONE, new ParticleSpawnerRainbowDust(), "rainbow", "pride"),
	SNOWBALL(Particle.SNOWBALL, new DefaultParticleSpawner(1)),
	SNOW_SHOVEL(Particle.SNOW_SHOVEL, new DefaultParticleSpawner(1)),
	SLIME(Particle.SLIME, new DefaultParticleSpawner(1), "goo"),
	HEART(Particle.HEART, new DefaultOffsetParticleSpawner(4), "hearts"),
	BARRIER(Particle.BARRIER, new DefaultParticleSpawner(1)),
	WATER_DROP(Particle.WATER_DROP, new DefaultParticleSpawner(1)),
	DRAGON_BREATH(Particle.DRAGON_BREATH, new DefaultOffsetParticleSpawner(8)),
	END_ROD(Particle.END_ROD, new DefaultParticleSpawner(1)),
	DAMAGE_INDICATOR(Particle.DAMAGE_INDICATOR, new DefaultOffsetParticleSpawner(4), "dark_hearts"),
	SWEEP_ATTACK(Particle.SWEEP_ATTACK, new DefaultParticleSpawner(1)),
	TOTEM(Particle.TOTEM, new DefaultParticleSpawner(1)),
	SPIT(Particle.SPIT, new DefaultParticleSpawner(1)),
	SQUID_INK(Particle.SQUID_INK, new DefaultOffsetParticleSpawner(4)),
	BUBBLE_POP(Particle.BUBBLE_POP, new DefaultParticleSpawner(1)),
	CURRENT_DOWN(Particle.CURRENT_DOWN, new DefaultOffsetParticleSpawner(8)),
	BUBBLE_COLUMN_UP(Particle.BUBBLE_COLUMN_UP, new DefaultOffsetParticleSpawner(8)),
	NAUTILUS(Particle.NAUTILUS, new DefaultParticleSpawner(1)),
	DOLPHIN(Particle.DOLPHIN, new DefaultOffsetParticleSpawner(16)),
	SNEEZE(Particle.SNEEZE, new DefaultParticleSpawner(1)),
	CAMPFIRE_COSY_SMOKE(Particle.CAMPFIRE_COSY_SMOKE, new DefaultOffsetParticleSpawner(4)),
	CAMPFIRE_SIGNAL_SMOKE(Particle.CAMPFIRE_SIGNAL_SMOKE, new DefaultParticleSpawner(1)),
	COMPOSTER(Particle.COMPOSTER, new DefaultOffsetParticleSpawner(8)),
	FALLING_LAVA(Particle.FALLING_LAVA, new DefaultParticleSpawner(1)),
	FALLING_LAVA_CLUSTER(Particle.FALLING_LAVA, new DefaultOffsetParticleSpawner(8)),
	LANDING_LAVA(Particle.LANDING_LAVA, new DefaultParticleSpawner(1)),
	LANDING_LAVA_CLUSTER(Particle.LANDING_LAVA, new DefaultOffsetParticleSpawner(8)),
	FALLING_WATER(Particle.FALLING_WATER, new DefaultParticleSpawner(1)),
	FALLING_WATER_CLUSTER(Particle.FALLING_WATER, new DefaultOffsetParticleSpawner(8)),
	DRIPPING_HONEY(Particle.DRIPPING_HONEY, new DefaultParticleSpawner(1)),
	DRIPPING_HONEY_CLUSTER(Particle.DRIPPING_HONEY, new DefaultOffsetParticleSpawner(8)),
	FALLING_HONEY(Particle.FALLING_HONEY, new DefaultParticleSpawner(1)),
	FALLING_HONEY_CLUSTER(Particle.FALLING_HONEY, new DefaultOffsetParticleSpawner(8)),
	LANDING_HONEY(Particle.LANDING_HONEY, new DefaultParticleSpawner(1)),
	LANDING_HONEY_CLUSTER(Particle.LANDING_HONEY, new DefaultOffsetParticleSpawner(8)),
	FALLING_NECTAR(Particle.FALLING_NECTAR, new DefaultParticleSpawner(1)),
	FALLING_NECTAR_CLUSTER(Particle.FALLING_NECTAR, new DefaultOffsetParticleSpawner(8)),
	SOUL_FIRE_FLAME(Particle.SOUL_FIRE_FLAME, new DefaultParticleSpawner(1), "soul_fire", "blue_fire", "fire_blue"),
	SOUL_FIRE_FLAME_CLUSTER(Particle.SOUL_FIRE_FLAME,
			new DefaultOffsetParticleSpawner(4),
			"soul_fire_cluster",
			"blue_fire_cluster",
			"fire_blue_cluster"
	),
	ASH(Particle.ASH, new DefaultOffsetParticleSpawner(16)),
	CRIMSON_SPORE(Particle.CRIMSON_SPORE, new DefaultOffsetParticleSpawner(16)),
	WARPED_SPORE(Particle.WARPED_SPORE, new DefaultParticleSpawner(1)),
	SOUL(Particle.SOUL, new DefaultParticleSpawner(1)),
	SOUL_CLUSTER(Particle.SOUL, new DefaultOffsetParticleSpawner(8)),
	DRIPPING_OBSIDIAN_TEAR(Particle.DRIPPING_OBSIDIAN_TEAR, new DefaultParticleSpawner(1)),
	DRIPPING_OBSIDIAN_TEAR_CLUSTER(Particle.DRIPPING_OBSIDIAN_TEAR, new DefaultOffsetParticleSpawner(8)),
	FALLING_OBSIDIAN_TEAR(Particle.FALLING_OBSIDIAN_TEAR, new DefaultParticleSpawner(1)),
	FALLING_OBSIDIAN_TEAR_CLUSTER(Particle.FALLING_OBSIDIAN_TEAR, new DefaultOffsetParticleSpawner(8)),
	LANDING_OBSIDIAN_TEAR(Particle.LANDING_OBSIDIAN_TEAR, new DefaultParticleSpawner(1)),
	LANDING_OBSIDIAN_TEAR_CLUSTER(Particle.LANDING_OBSIDIAN_TEAR, new DefaultOffsetParticleSpawner(8)),
	REVERSE_PORTAL(Particle.REVERSE_PORTAL, new DefaultOffsetParticleSpawner(8)),
	WHITE_ASH(Particle.WHITE_ASH, new DefaultOffsetParticleSpawner(16));
	
	public static final String BASE_PERMISSION = "particleboots.enchantment";
	public static final String WILDCARD_PERMISSION = BASE_PERMISSION + ".*";
	
	private final Particle particle;
	private final ParticleSpawner spawner;
	private final Set<String> aliases = new HashSet<>();
	private final String permission;
	private final String natural_name = naturalizeName(name());
	
	
	BootParticle(Particle particle, ParticleSpawner spawner, String... aliases) {
		this.particle = particle;
		this.spawner = spawner;
		this.aliases.addAll(Arrays.asList(aliases));
		this.permission = BASE_PERMISSION + "." + name().toLowerCase();
	}
	
	private static String naturalizeName(String name) {
		return name.replace("_", "").toLowerCase();
	}
	
	public static BootParticle get(String name) {
		String natural_name = naturalizeName(name);
		for (BootParticle particles : values()) {
			if (particles.natural_name.equals(natural_name)) {
				return particles;
			}
			for (String alias : particles.aliases) {
				if (naturalizeName(alias).equals(natural_name)) {
					return particles;
				}
			}
		}
		throw new IllegalArgumentException("No particle found named \"" + name + "\"");
	}
	
	public Particle getParticle() {
		return particle;
	}
	
	public ParticleSpawner getSpawner() {
		return spawner;
	}
	
	public Set<String> getAliases() {
		return aliases;
	}
	
	public String getPermission() {
		return permission;
	}
	
	public abstract static class ParticleSpawner {
		protected final Random random = new Random();
		
		public abstract void spawnParticles(Particle particle, Location location);
	}
	
	public static class DefaultParticleSpawner extends DefaultOffsetParticleSpawner {
		
		public DefaultParticleSpawner(int count) {
			this(count, null);
		}
		
		public DefaultParticleSpawner(int count, Object data) {
			super(count, data, 0.0, 0.25, 0.0, 0.0, 0.0, 0.0, 0.01);
		}
	}
	
	public static class DefaultExtraParticleSpawner extends DefaultOffsetParticleSpawner {
		public DefaultExtraParticleSpawner(int count, double extra) {
			this(count, extra, null);
		}
		
		public DefaultExtraParticleSpawner(int count, double extra, Object data) {
			super(count, data, 0.0, 0.25, 0.0, 0.0, 0.0, 0.0, extra);
		}
	}
	
	public static class DefaultOffsetParticleSpawner extends ParticleSpawner {
		private final int count;
		private final Object data;
		private final double offset_x;
		private final double offset_y;
		private final double offset_z;
		private final double random_offset_x;
		private final double random_offset_y;
		private final double random_offset_z;
		private final double extra;
		
		public DefaultOffsetParticleSpawner(int count) {
			this(count, null);
		}
		
		public DefaultOffsetParticleSpawner(int count, Object data) {
			this(count, data, 0.0, 0.25, 0.0, 0.25, 0.25, 0.25, 0.01);
		}
		
		public DefaultOffsetParticleSpawner(int count, double offset_x, double offset_y, double offset_z,
											double random_offset_x, double random_offset_y, double random_offset_z,
											double extra) {
			this(count, null, offset_x, offset_y, offset_z, random_offset_x, random_offset_y, random_offset_z, extra);
		}
		
		public DefaultOffsetParticleSpawner(int count, Object data, double offset_x, double offset_y, double offset_z,
											double random_offset_x, double random_offset_y, double random_offset_z,
											double extra) {
			this.count = count;
			this.data = data;
			this.offset_x = offset_x;
			this.offset_y = offset_y;
			this.offset_z = offset_z;
			this.random_offset_x = random_offset_x;
			this.random_offset_y = random_offset_y;
			this.random_offset_z = random_offset_z;
			this.extra = extra;
		}
		
		@SuppressWarnings("ConstantConditions")
		@Override
		public void spawnParticles(Particle particle, Location location) {
			location.getWorld().spawnParticle(particle,
					location.getX() + offset_x,
					location.getY() + offset_y,
					location.getZ() + offset_z,
					count,
					random_offset_x,
					random_offset_y,
					random_offset_z,
					extra,
					data
			);
		}
	}
}
