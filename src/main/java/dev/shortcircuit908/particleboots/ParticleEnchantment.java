package dev.shortcircuit908.particleboots;

import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.inventory.ItemStack;

public class ParticleEnchantment extends Enchantment {
	public ParticleEnchantment(ParticleBootsPlugin plugin) {
		super(new NamespacedKey(plugin, "particles"));
	}
	
	@Override
	public String getName() {
		return "Particles";
	}
	
	@Override
	public int getMaxLevel() {
		return BootParticle.values().length + 1;
	}
	
	@Override
	public int getStartLevel() {
		return 1;
	}
	
	@Override
	public EnchantmentTarget getItemTarget() {
		return EnchantmentTarget.ARMOR_FEET;
	}
	
	@Override
	public boolean isTreasure() {
		return false;
	}
	
	@Override
	public boolean isCursed() {
		return false;
	}
	
	@Override
	public boolean conflictsWith(Enchantment other) {
		return false;
	}
	
	@Override
	public boolean canEnchantItem(ItemStack item) {
		return EnchantmentTarget.ARMOR_FEET.includes(item);
	}
}
