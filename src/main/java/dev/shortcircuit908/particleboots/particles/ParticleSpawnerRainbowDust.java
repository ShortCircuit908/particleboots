package dev.shortcircuit908.particleboots.particles;

import dev.shortcircuit908.particleboots.BootParticle;
import dev.shortcircuit908.particleboots.ColorHelper;
import org.bukkit.Location;
import org.bukkit.Particle;

import java.util.Random;

public class ParticleSpawnerRainbowDust extends BootParticle.ParticleSpawner {
	private static final Random random = new Random(System.currentTimeMillis());
	private static final int period = 5000;
	private static final int steps = 64;
	private static final int ms_per_step = period / steps;
	private static final Particle.DustOptions[] dust_options = new Particle.DustOptions[steps];
	
	
	static {
		for (int i = 0; i < steps; i++) {
			float hue = (1.0f / steps) * i;
			dust_options[i] =
					new Particle.DustOptions(ColorHelper.hsvToRgb(hue, 1.0f, 1.0f),
							(random.nextFloat() * 0.5f) + 0.5f);
		}
	}
	
	@SuppressWarnings("ConstantConditions")
	@Override
	public void spawnParticles(Particle particle, Location location) {
		int slice = (int) (((double) (System.currentTimeMillis() % period)) / ms_per_step);
		location.getWorld().spawnParticle(particle,
				location.getX(),
				location.getY() + 0.25,
				location.getZ(),
				16,
				0.25,
				0.25,
				0.25,
				0.01,
				dust_options[slice]
		);
	}
}
