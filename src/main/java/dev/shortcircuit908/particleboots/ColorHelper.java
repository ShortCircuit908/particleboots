package dev.shortcircuit908.particleboots;

import org.bukkit.Color;

public class ColorHelper {
	public static Color hsvToRgb(float hue, float saturation, float value) {
		int h = (int) (hue * 6);
		float f = hue * 6 - h;
		float p = value * (1 - saturation);
		float q = value * (1 - f * saturation);
		float t = value * (1 - (1 - f) * saturation);
		
		q *= 255;
		p *= 255;
		t *= 255;
		value *= 255;
		
		switch (h) {
			case 0:
				return Color.fromRGB((int) value, (int) t, (int) p);
			case 1:
				return Color.fromRGB((int) q, (int) value, (int) p);
			case 2:
				return Color.fromRGB((int) p, (int) value, (int) t);
			case 3:
				return Color.fromRGB((int) p, (int) q, (int) value);
			case 4:
				return Color.fromRGB((int) t, (int) p, (int) value);
			case 5:
				return Color.fromRGB((int) value, (int) p, (int) q);
			default:
				throw new RuntimeException(
						"Something went wrong when converting from HSV to RGB. Input was " + hue + ", " + saturation +
								", " + value);
		}
	}
}
