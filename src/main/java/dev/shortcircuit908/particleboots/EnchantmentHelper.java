package dev.shortcircuit908.particleboots;

import java.lang.reflect.Field;
import org.bukkit.enchantments.Enchantment;

public class EnchantmentHelper {
	public static void registerEnchantment(Enchantment enchantment) {
		try {
			Field field_acceptingNew = Enchantment.class.getDeclaredField("acceptingNew");
			field_acceptingNew.setAccessible(true);
			field_acceptingNew.set(null, true);
			Enchantment.registerEnchantment(enchantment);
		}
		catch (ReflectiveOperationException e) {
			e.printStackTrace();
		}
	}
	
	
}
