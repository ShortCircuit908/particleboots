package dev.shortcircuit908.particleboots;

import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.plugin.PluginLoadOrder;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.annotation.command.Command;
import org.bukkit.plugin.java.annotation.permission.Permission;
import org.bukkit.plugin.java.annotation.plugin.ApiVersion;
import org.bukkit.plugin.java.annotation.plugin.Description;
import org.bukkit.plugin.java.annotation.plugin.LoadOrder;
import org.bukkit.plugin.java.annotation.plugin.Plugin;
import org.bukkit.plugin.java.annotation.plugin.author.Author;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Plugin(name = "ParticleBoots", version = "1.0.0")
@ApiVersion(ApiVersion.Target.v1_15)
@Description("Shiny particle trails")
@LoadOrder(value = PluginLoadOrder.STARTUP)
@Author("ShortCircuit908")
@Command(name = "particleboots",
		aliases = {"pboots", "pboot", "particleboot"},
		permission = "particleboots.enchant",
		usage = "/<command> <particle>",
		desc = "Enchant boots with particle effects")
@Command(name = "particlebootslist",
		aliases = {"pbootslist", "pbootlist", "particlebootlist", "particlelist", "listparticles"},
		permission = "particleboots.enchant",
		usage = "/<command> [search] [p:page]",
		desc = "List available particle effects")
@Permission(name = "particleboots.enchantment.*",
		desc = "Wildcard particles permission",
		defaultValue = PermissionDefault.OP)
@Permission(name = "particleboots.enchant",
		desc = "Grants access to the particle command",
		defaultValue = PermissionDefault.OP)
public class ParticleBootsPlugin extends JavaPlugin {
	private final Enchantment particle_enchantment = new ParticleEnchantment(this);
	private final YamlConfiguration alias_config = new YamlConfiguration();
	private final File alias_config_file = getDataFolder().toPath().resolve("aliases.yml").toFile();
	
	@Override
	public void onLoad() {
		if (Enchantment.getByKey(particle_enchantment.getKey()) == null) {
			EnchantmentHelper.registerEnchantment(particle_enchantment);
		}
	}
	
	@Override
	public void onDisable() {
	
	}
	
	@SuppressWarnings("ConstantConditions")
	@Override
	public void onEnable() {
		getServer().getPluginManager().registerEvents(new ParticleListener(this), this);
		PluginCommand pl_cmd_particleboots = getCommand("particleboots");
		ParticleCommand command_particleboots = new ParticleCommand(this);
		pl_cmd_particleboots.setExecutor(command_particleboots);
		pl_cmd_particleboots.setTabCompleter(command_particleboots);
		getCommand("particlebootslist").setExecutor(new ParticleListCommand());
		try{
			loadAliases();
		}
		catch (IOException | InvalidConfigurationException e){
			getLogger().warning("Failed to load particle aliases");
			e.printStackTrace();
		}
	}
	
	private void loadAliases() throws IOException, InvalidConfigurationException {
		if(!alias_config_file.exists()){
			for(BootParticle particle : BootParticle.values()){
				alias_config.set(particle.name().toLowerCase(), new ArrayList<>(particle.getAliases()));
			}
			alias_config.save(alias_config_file);
			return;
		}
		else{
			alias_config.load(alias_config_file);
		}
		for(BootParticle particle : BootParticle.values()){
			List<String> aliases = alias_config.getStringList(particle.name().toLowerCase());
			particle.getAliases().addAll(aliases);
		}
	}
	
	public Enchantment getParticleEnchantment() {
		return particle_enchantment;
	}
}
